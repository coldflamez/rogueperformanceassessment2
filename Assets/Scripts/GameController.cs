﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class GC : MonoBehaviour {
	
	public static GC Instance;
	public bool isPlayerTurn;
	public bool areEnemiesMoving;
	public int playerCurrentHealth = 50;
	public AudioClip gameOverSound;
	
	private BoardController boardController;
	private List<Enemy> enemies;
	private GameObject levelImage;
	private Text levelText;
	private bool settingUpGame;
	private int secondsUntilLevelStart = 2;
	private int currentLevel = 0;
	private GameObject StartImage;
	private Text StartText;
	
	public void StartLevel()
	{
		Application.LoadLevel(1);
	}
	
	void Awake () {
		if(Instance != null && Instance != this)
		{
			DestroyImmediate(gameObject);
			return;
		}
		
		Instance = this;
		DontDestroyOnLoad(gameObject);
		boardController = GetComponent<BoardController>();
		enemies = new List<Enemy>();
	}
	
	void Start()
	{
		StartGame();
	}
	
	public void StartGame() {
		settingUpGame = true;
		StartImage = GameObject.Find("Start Image");
		StartText = GameObject.Find("Start Text").GetComponent<Text>();
		StartImage.SetActive(true);
		Debug.Log("Start Game");
	}
	
	public void DisableStartImage(){
		StartImage.SetActive(false);
		settingUpGame = false;
		isPlayerTurn = true;
		areEnemiesMoving = false;
		InitializeGame();
		Debug.Log("initialize Game");
	}
	
	private void InitializeGame()
	{
		if (currentLevel > 0)
		{
			StartImage = GameObject.Find("Start Image");
			StartImage.SetActive(false);
		}
		settingUpGame = true;
		levelImage = GameObject.Find("Level Image");
		levelText = GameObject.Find("Level Text").GetComponent<Text>();
		levelText.text = "Day " + currentLevel;
		levelImage.SetActive(true);
		enemies.Clear();
		boardController.SetupLevel(currentLevel);
		Invoke("DisableLevelImage", secondsUntilLevelStart);
	}
	
	private void DisableLevelImage()
	{
		levelImage.SetActive(false);
		settingUpGame = false;
		isPlayerTurn = true;
		areEnemiesMoving = false;
	}


}