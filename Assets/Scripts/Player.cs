﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Player : MovingObject {

	private int healthPerCoin = 5;
	private int healthPerEnemy = 5;

	private int xAxis = 0;
	private int yAxis = 0;
	
	private int attackPower = 2;

	/*public float maxSpeed = 3;

	void Update () {
		int xAxis = 0;
		int yAxis = 0;

		xAxis = (int)Input.GetAxisRaw("Horizontal");
		yAxis = (int)Input.GetAxisRaw("Vertical");

		CanObjectMove(xAxis, yAxis);

	}*/

	public Text healthText; 

	public float speed = 2f;
	private Animator animator;
	private int playerHealth = 50;
	public float RotateSpeed = 30f;
	

	// Update is called once per frame

	protected override void Start() {
		base.Start ();
		animator = GetComponent<Animator> ();
		healthText.text = "Health: " + playerHealth;
	}

	void Update () {

		if (xAxis != 0 || yAxis != 0)
		{
			playerHealth--;
			healthText.text = "Health: " + playerHealth;
			// SoundController.Instance.PlaySingle(movementSound1, movementSound2);
			Move<Wall>(xAxis, yAxis);
			GameController.Instance.isPlayerTurn = false;
		}

	
	if (Input.GetKey (KeyCode.R))
		transform.Rotate (-Vector3.up * RotateSpeed * Time.deltaTime);
		else if (Input.GetKey (KeyCode.L))
			transform.Rotate (Vector3.up * RotateSpeed * Time.deltaTime);


	     if (Input.GetKeyDown ("space")) {
			transform.Translate (Vector3.up * 300 * Time.deltaTime, Space.World);
		}
		else if (Input.GetKey (KeyCode.RightArrow)) {
			transform.Translate(Vector2.right * speed * Time.deltaTime);
		}
		else if (Input.GetKey (KeyCode.LeftArrow)) {
			transform.Translate(-Vector2.right * speed * Time.deltaTime); 
		}
		else if (Input.GetKey (KeyCode.UpArrow)) {
			transform.Translate(Vector2.up * speed * Time.deltaTime);
		}
		else if (Input.GetKey (KeyCode.DownArrow)) {
			transform.Translate(-Vector2.up * speed * Time.deltaTime);  
		}

	} 
	private void OnTriggerEnter2D(Collider2D objectPlayerCollidedWith)
	{
		if (objectPlayerCollidedWith.tag == "Coin") {
			Debug.Log ("Coin");
			 playerHealth += healthPerCoin;
			 healthText.text = "+" + healthPerCoin + " Health\n" + "Health: " + playerHealth;
			objectPlayerCollidedWith.gameObject.SetActive (false);
		    // SoundController.Instance.PlaySingle (fruitSound1, fruitSound2);
		}
		else if (objectPlayerCollidedWith.tag == "Enemy") {
			Debug.Log ("Enemy");
			playerHealth += healthPerEnemy;
			healthText.text = "-" + healthPerEnemy + " Health\n" + "Health: " + playerHealth;
			objectPlayerCollidedWith.gameObject.SetActive (false);
		}
	}
		protected override void HandleCollision<T>(T component)
		{
		Wall wall = component as Wall;
		animator.SetTrigger("player-attack");
		// SoundController.Instance.PlaySingle(Player);
		wall.DamageWall(attackPower);
	}
	
	public void TakeDamage(int damageReceived) {
		playerHealth--;
		healthText.text = "Health: " + playerHealth;
		playerHealth -= damageReceived;
		animator.SetTrigger ("player-attack");
	}
	
}

