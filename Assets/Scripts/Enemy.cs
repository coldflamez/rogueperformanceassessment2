﻿using UnityEngine;
using System.Collections;
	
	public class Enemy : MovingObject {
	private bool dirRight = true;
	public float speed = 2.0f;

	public int attackDamage;
	private Animator animator;
	private int playerHealth;
	private Transform player;

	protected override void Start () {
		GameController.Instance.AddEnemyToList(this);
		player = GameObject.FindGameObjectWithTag("Player").transform;
		//skipCurrentMove = true;
		animator = GetComponent<Animator>();
		base.Start();
	}
	
	
	void Update () {
		if(dirRight)
			transform.Translate(Vector2.right * speed * Time.deltaTime);
		else
			transform.Translate(-Vector2.right * speed * Time.deltaTime);

		if(transform.position.x >= 1f) {
			dirRight = false;
		}

		if(transform.position.x <= -1) {
			dirRight = true;
		}
	}
	public void MoveEnemy() {
		// Move<Player>(xAxis, yAxis);
	}

	protected override void HandleCollision<T>(T component)
	{
		  Player player = component as Player;
		  player.TakeDamage (attackDamage);
		  animator.SetTrigger("enemyAttack");
	}

}